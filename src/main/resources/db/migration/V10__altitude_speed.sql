ALTER TABLE "position" ALTER COLUMN longitude TYPE double precision;
ALTER TABLE "position" ALTER COLUMN latitude TYPE double precision;

ALTER TABLE "position" ADD COLUMN speed real;
ALTER TABLE "position" ADD COLUMN altitude double precision;
ALTER TABLE "position" ADD COLUMN accuracy real;

