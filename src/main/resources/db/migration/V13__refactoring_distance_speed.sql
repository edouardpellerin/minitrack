ALTER TABLE player ADD COLUMN instant_speed float;
ALTER TABLE position DROP COLUMN map_id;
CREATE INDEX idx_position_player_timestamp ON position(timestamp, player_id);
ALTER TABLE map DROP COLUMN scrubbed;