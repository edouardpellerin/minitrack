ALTER TABLE position
    ADD COLUMN accuracy float;

ALTER TABLE position
    ADD COLUMN speed float;
