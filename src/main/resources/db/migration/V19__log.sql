CREATE TABLE log
(
  id      uuid primary key,
  player_id  bigint references player (id) on delete cascade,
  level   text,
  message text,
  date    timestamp
);
