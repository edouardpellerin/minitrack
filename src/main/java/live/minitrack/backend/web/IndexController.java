package live.minitrack.backend.web;

import jakarta.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.CacheControl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
@Slf4j
public class IndexController {

  @GetMapping
  public String index(HttpServletResponse response) {
    String headerValue = CacheControl.maxAge(1, TimeUnit.DAYS).getHeaderValue();
    response.addHeader("Cache-Control", headerValue);
    return "index";
  }
}
