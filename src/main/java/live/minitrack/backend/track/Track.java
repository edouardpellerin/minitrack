package live.minitrack.backend.track;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import java.util.List;
import live.minitrack.backend.map.Map;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Track {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne @JsonBackReference private Map map;

  @OneToMany(mappedBy = "track")
  @OrderBy("time")
  private List<TrackPosition> trackPositions;
}
