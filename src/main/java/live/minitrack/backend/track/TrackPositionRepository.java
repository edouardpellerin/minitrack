package live.minitrack.backend.track;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackPositionRepository extends JpaRepository<TrackPosition, Long> {}
