package live.minitrack.backend.position;

import java.time.LocalDateTime;
import java.util.List;
import live.minitrack.backend.map.Map;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Long> {
  List<Position> findByPlayerMapOrderByPlayerNameAscTimestampAsc(Map map);

  List<Position> findByPlayerMapAndTimestampAfterOrderByPlayerNameAscTimestampAsc(
      Map map, LocalDateTime timestamp);

  List<Position> findPositionByPlayerIdOrderByTimestampAsc(long id);
}
