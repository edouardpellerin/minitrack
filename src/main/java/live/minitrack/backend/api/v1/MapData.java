package live.minitrack.backend.api.v1;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
class MapData {
  private List<PlayerData> data = new ArrayList<>();
  private List<PointOfInterestData> pointsOfInterest = new ArrayList<>();
}
