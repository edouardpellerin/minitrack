package live.minitrack.backend.api.v1;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class MapRequest {
  @NotBlank private String title;

  @NotBlank private String playerName;
}
