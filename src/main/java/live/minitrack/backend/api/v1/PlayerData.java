package live.minitrack.backend.api.v1;

import java.util.List;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
class PlayerData {
  private String name;
  private List<Double[]> positions;
  private String lastTimestamp;
  private String distance;
  private String instantSpeed;
}
