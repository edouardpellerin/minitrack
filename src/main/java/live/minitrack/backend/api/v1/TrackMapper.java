package live.minitrack.backend.api.v1;

import java.util.ArrayList;
import java.util.List;
import live.minitrack.backend.track.Track;
import live.minitrack.backend.track.TrackPosition;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface TrackMapper {

  @Mapping(target = "positions", ignore = true)
  TrackDto map(Track track);

  List<TrackDto> map(List<Track> tracks);

  @AfterMapping
  default void afterMapping(@MappingTarget TrackDto trackDto, Track track) {
    List<Double[]> positions = new ArrayList<>();
    for (TrackPosition position : track.getTrackPositions()) {
      positions.add(new Double[] {position.getLatitude(), position.getLongitude()});
    }
    trackDto.setPositions(positions);
  }
}
