package live.minitrack.backend.api.v1;

import jakarta.validation.Valid;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import live.minitrack.backend.map.Map;
import live.minitrack.backend.map.MapCreation;
import live.minitrack.backend.map.MapRepository;
import live.minitrack.backend.map.MapService;
import live.minitrack.backend.model.Player;
import live.minitrack.backend.model.PlayerRepository;
import live.minitrack.backend.model.PointOfInterest;
import live.minitrack.backend.position.Position;
import live.minitrack.backend.position.PositionRepository;
import live.minitrack.backend.position.PositionService;
import live.minitrack.backend.track.TrackService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/api/map", "/api/v1/map"})
@RequiredArgsConstructor
public class MapApiController {

  private final PositionMapper positionMapper;

  private final SecureRandom random = new SecureRandom();
  private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_TIME;
  private final PlayerRepository playerRepository;
  private final MapRepository mapRepository;
  private final MapService mapService;
  private final TrackService trackService;
  private final PositionRepository positionRepository;
  private final PositionService positionService;
  private final TrackMapper trackMapper;
  private final DecimalFormat decimalFormat = new DecimalFormat("###.##");

  @PostMapping
  public ResponseEntity<JoinResponse> createMap(
      @Valid @RequestBody MapRequest mapRequest, Errors errors) {
    if (!errors.hasErrors()) {
      MapCreation mapCreation =
          mapService.createMap(mapRequest.getPlayerName(), mapRequest.getTitle());

      MapResponse mapResponse =
          MapResponse.builder()
              .title(mapCreation.getMap().getTitle())
              .key(mapCreation.getMap().getKey())
              .build();

      JoinResponse joinResponse =
          JoinResponse.builder()
              .map(mapResponse)
              .playerToken(mapCreation.getPlayer().getToken())
              .build();
      return new ResponseEntity<>(joinResponse, HttpStatus.CREATED);
    } else {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(value = "/{key}/data")
  @ResponseBody
  public MapData data(@PathVariable String key) {
    MapData mapData = new MapData();
    Optional<Map> map = mapRepository.findByKey(key);
    if (map.isPresent()) {

      List<Position> positions =
          positionRepository.findByPlayerMapOrderByPlayerNameAscTimestampAsc(map.get());

      HashMap<Player, List<Position>> positionsByPlayer = new HashMap<>();
      for (Position position : positions) {
        Player player;
        if (position.getPlayer() == null) {
          player = Player.builder().name("default").build();
        } else {
          player = position.getPlayer();
        }
        if (!positionsByPlayer.containsKey(player)) {
          positionsByPlayer.put(player, new ArrayList<>());
        }
        positionsByPlayer.get(player).add(position);
      }

      for (Player player : positionsByPlayer.keySet()) {
        List<Position> positionList = positionsByPlayer.get(player);
        LocalDateTime dateTime =
            positionsByPlayer.get(player).get(positionList.size() - 1).getTimestamp();
        float distance =
            positionList.stream()
                .map(Position::getDistance)
                .filter(Objects::nonNull)
                .reduce(0f, Float::sum);
        PlayerData playerData =
            PlayerData.builder()
                .name(player.getName())
                .positions(
                    positionList.stream()
                        .map(p -> new Double[] {p.getLatitude(), p.getLongitude()})
                        .collect(Collectors.toList()))
                .lastTimestamp(dateTimeFormatter.format(dateTime))
                .distance(decimalFormat.format(distance / 1000))
                .instantSpeed(
                    player.getInstantSpeed() != null
                        ? decimalFormat.format(player.getInstantSpeed())
                        : "0.00")
                .build();
        mapData.getData().add(playerData);
      }

      for (PointOfInterest pointOfInterest : map.get().getPointOfInterestList()) {
        PointOfInterestData pointOfInterestData = new PointOfInterestData();
        pointOfInterestData.setDescription(pointOfInterest.getDescription());
        pointOfInterestData.setPosition(
            new double[] {pointOfInterest.getLatitude(), pointOfInterest.getLongitude()});
        pointOfInterestData.setType(pointOfInterest.getType());
        mapData.getPointsOfInterest().add(pointOfInterestData);
      }
    }
    return mapData;
  }

  @PostMapping(value = "/{key}/position")
  public ResponseEntity<Void> postList(
      @RequestBody PlayerUpdate playerUpdate, @PathVariable String key) {
    Optional<Player> player =
        playerRepository.findByMapKeyAndToken(key, playerUpdate.getPlayerToken());
    if (player.isPresent()) {
      List<Position> positions =
          playerUpdate.getPositions().stream()
              .map(positionMapper::map)
              .collect(Collectors.toList());
      positionService.updatePositions(player.get(), positions);
      positionService.updateDistanceAndSpeed(player.get());
      return ResponseEntity.ok().build();
    } else {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping(value = "/{key}/share", consumes = "text/plain")
  public ResponseEntity<String> shareMap(
      @PathVariable String key, @RequestBody String playerToken) {
    Optional<Player> player = playerRepository.findByMapKeyAndToken(key, playerToken);
    if (player.isPresent()) {
      StringBuilder sb = new StringBuilder(6);
      for (int i = 0; i < 6; i++) {
        sb.append(random.nextInt(10));
      }
      Map map = player.get().getMap();
      if (StringUtils.isBlank(map.getSharingCode())) {
        map.setSharingCode(sb.toString());
      }
      mapRepository.save(map);
      return new ResponseEntity<>(map.getSharingCode(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping(value = "/join")
  public ResponseEntity<JoinResponse> joinMap(@RequestBody JoinRequest joinRequest) {
    Optional<Map> map = mapRepository.findBySharingCode(joinRequest.getCode());
    if (map.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    Player player = new Player();
    player.setName(joinRequest.getName());
    player.setMap(map.get());
    player.setToken(new BigInteger(40, random).toString(32));
    playerRepository.save(player);

    MapResponse mapResponse =
        MapResponse.builder().key(map.get().getKey()).title(map.get().getTitle()).build();
    return ResponseEntity.ok(
        JoinResponse.builder()
            .map(mapResponse)
            .playerToken(player.getToken())
            .name(player.getName())
            .build());
  }

  @PostMapping(value = "/{key}/track")
  public ResponseEntity<Void> track(@PathVariable String key, @RequestBody String gpx) {
    mapService.addTrack(key, gpx);
    return ResponseEntity.ok().build();
  }

  @GetMapping(value = "/{key}/tracks")
  public ResponseEntity<List<TrackDto>> getTracks(@PathVariable String key) {
    Optional<Map> optionalMap = mapRepository.findByKey(key);
    return optionalMap
        .map(map -> ResponseEntity.ok(trackMapper.map(trackService.getTracksForMap(map))))
        .orElseGet(() -> ResponseEntity.ok(new ArrayList<>()));
  }
}
