package live.minitrack.backend.log;

import java.time.LocalDateTime;
import java.util.Optional;
import live.minitrack.backend.model.Player;
import live.minitrack.backend.model.PlayerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/logs")
@RequiredArgsConstructor
public class LogApi {

  private final LogRepository logRepository;
  private final PlayerRepository playerRepository;

  @PostMapping
  public HttpStatus newLog(@RequestBody LogRequest logRequest) {
    Optional<Player> player =
        playerRepository.findByMapKeyAndToken(logRequest.map(), logRequest.token());

    if (player.isEmpty()) {
      return HttpStatus.NOT_FOUND;
    }

    player.ifPresent(
        p -> {
          Log log = new Log();
          log.setPlayer(p);
          log.setMessage(logRequest.message());
          log.setLevel(logRequest.level());
          log.setDate(LocalDateTime.now());
          logRepository.save(log);
        });

    return HttpStatus.OK;
  }
}
