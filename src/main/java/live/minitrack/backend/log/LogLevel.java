package live.minitrack.backend.log;

public enum LogLevel {
  INFO,
  WARNING,
  ERROR
}
