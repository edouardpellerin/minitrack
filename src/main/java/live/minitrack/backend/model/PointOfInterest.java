package live.minitrack.backend.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import live.minitrack.backend.map.Map;
import lombok.Data;

@Entity
@Data
public class PointOfInterest {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank private String description;

  @NotNull private Double latitude;

  @NotNull private Double longitude;

  @ManyToOne private Map map;

  @Enumerated(EnumType.STRING)
  @NotNull private PointOfInterestType type;
}
