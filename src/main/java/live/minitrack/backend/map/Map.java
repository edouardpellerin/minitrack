package live.minitrack.backend.map;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;
import live.minitrack.backend.model.Player;
import live.minitrack.backend.model.PointOfInterest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Map {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank private String title;

  @NotBlank
  @Column(unique = true)
  private String key;

  @NotNull private ZonedDateTime creationDate;

  @OneToMany(mappedBy = "map", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Player> players;

  private String sharingCode;

  private boolean permanent;

  @OneToMany(mappedBy = "map", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PointOfInterest> pointOfInterestList;
}
