package live.minitrack.backend.api.v1;

import static io.restassured.RestAssured.with;

import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MapApiControllerTest {

  private MockMvc mockMvc;

  @LocalServerPort private int port;

  @BeforeEach
  void setUp() {
    RestAssuredMockMvc.mockMvc(mockMvc);
  }

  @Test
  void createMap() {
    var json =
        """
        {
            "title": "test",
            "playerName": "Edouard"
        }
        """;
    with()
        .body(json)
        .contentType(ContentType.JSON)
        .port(port)
        .post("/api/v1/map")
        .then()
        .statusCode(201);

    json = """
        {
        }
        """;
    with()
        .body(json)
        .contentType(ContentType.JSON)
        .port(port)
        .post("/api/v1/map")
        .then()
        .statusCode(400);
  }
}
